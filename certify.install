<?php

/**
 * @file
 * Installs the Certify module
 */

/**
 * Implementation of hook_requirements().
 */
function certify_requirements($phase) {
  $t = get_t();
  $requirements = array();
  $quizneeded = array(
    'title' => $t('Certify require Quiz v4'),
    'description' => $t('Quiz v4 or higher is required'),
    'severity' => REQUIREMENT_ERROR
  );

  if ($phase=="runtime" || $phase=="install") {

    // The following code checks the version of the quiz module. We need v4 or higher.

    // Get current list of modules
    $files = drupal_system_listing('\.module$', 'modules', 'name', 0);

    // Isolate the quiz module, since it's the only one we're interested in
    $quizmod = array('quiz' => $files['quiz']);
    unset($files);

    // More info
    system_get_files_database($quizmod, 'module');

    // Parse info file
    $quizmod['quiz']->info = drupal_parse_info_file(dirname($quizmod['quiz']->filename) .'/'. $quizmod['quiz']->name .'.info');

    // Invoke hook_system_info_alter() to give installed modules a chance to
    // modify the data in the .info files if necessary.
    drupal_alter('system_info', $quizmod['quiz']->info, $quizmod['quiz']);

    // Did we find the version number?
    if (!isset($quizmod['quiz']->info['version'])) {
      // Actually, if we don't find the version number, we're probably working on a CVS copy. Should be ok...
    }
    else {
      // Fetch and parse with regexp
      $ver = $quizmod['quiz']->info['version'];
      $matches = array();
      $match = preg_match('/\d+\.[\w\d]+-(\d+)\.[\w\d]+/', $ver, $matches);
      // Anything below 4 is a no-no
      if ($matches[1] < 4) {
        $quizneeded['value'] = $matches[1];
        $requirements['quizneeded'] = $quizneeded;
      }
    }
  }

  if ($phase=="runtime") {

    $certificate_path = variable_get('certify_certificate_path', FALSE);
    $pathreq = array(
      'title' => $t('Certify path required'),
      'description' => $t('Certificate path needs to be set to a directory writable by the webserver.'),
      'value' => $t("Not set or not writable."),
      'severity' => REQUIREMENT_ERROR
    );

    if (!$certificate_path || !is_dir($certificate_path)) {
      $requirements['pathexists'] = $pathreq;
    }
    if (!is_writable($certificate_path)) {
      $requirements['pathexists'] = $pathreq;
    }

    $pdftkpath = variable_get('certify_pdftk_path', FALSE);
    $pdftkreq = array(
      'title' => $t('Certify pdftk required'),
      'description' => $t('pdftk path needs to point to the pdftk executable on the server.'),
      'value' => $t("Not found."),
      'severity' => REQUIREMENT_ERROR
    );

    if (!file_exists($pdftkpath) || !is_executable($pdftkpath)) {
      $requirements['pdftkinstalled'] = $pdftkreq;
    }

    if (!count(certify_conditiontypes())) {
      $requirements['noconditions'] = array(
        'title' => t('No conditions enabled'),
        'description' => t('You haven\'t enabled any conditions for Certify. The module makes no sense without any.'),
        'value' => $t("None found."),
        'severity' => REQUIREMENT_WARNING
      );
    }

  }
  return $requirements;
}

/**
 * Implementation of hook_install().
 */
function certify_install() {
  drupal_install_schema('certify');
  variable_set('certify_certificate_path', '/var/drupal-content/certify/');
  variable_set('certify_og_filter', TRUE);
  variable_set('certify_pdftk_path', '/usr/bin/pdftk');
  // Date format defaults to first part of short date format (time info is discarded)
  $datefmt = explode(' ', variable_get('date_format_short', 'm/d/Y'));
  variable_set('certify_dateformat', $datefmt[0]);
  drupal_set_message(st("Certify settings are available under !link",
    array( '!link' => l(st('Administer > Site configuration > Certify'),  'admin/settings/certify/settings' ) )
  ));
}

/**
 * Implementation of hook_uninstall().
 */
function certify_uninstall() {
  drupal_uninstall_schema('certify');
  variable_del('certify_certificate_path');
  variable_del('certify_og_filter');
  variable_del('certify_pdftk_path');
  variable_del('certify_dateformat');
}

/**
 * Implementation of hook_schema().
 */
function certify_schema() {

  $schema['certify_conditions'] = array(
    'description' => t('Conditions used for issuing a certificate from {certify}'),
    'fields' => array(
      'cid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t("The certificates {node}.nid."),
      ),
      'vid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t("The certificates {node}.vid."),
      ),
      'condtype' => array(
        'type' => 'text',
        'not null' => TRUE,
        'description' => t('The condition type'),
      ),
      'condnode' => array(
        'type' => 'int',
        'description' => t('The node operated on by the condition'),
      ),
      'condnodev' => array(
        'type' => 'int',
        'description' => t('The node revision operated on by the condition'),
      ),
    ),
    'primary key' => array('vid', 'condnodev'),
  );

  return $schema;
}

/**
 * Implementation of hook_enable().
 */
function certify_enable() {
  drupal_flush_all_caches();
}

// !Database updates

/**
 * Update hook for certify 2.x
 *
 * "Moves" {certify_booklog} to {certify_book_viewlog}
 * (controlled by certify_book).
 */
function certify_update_6200() {
  $ret = array();
  if (! $ret[] = update_sql("ALTER TABLE {certify_booklog} RENAME TO {certify_book_viewlog}")) {
    if (db_table_exists('certify_book_viewlog')) {
      if ($ret = update_sql("INSERT INTO {certify_book_viewlog} SELECT * FROM {certify_booklog}")) {
        $ret = array($ret, update_sql("DROP TABLE {certify_booklog}"));
      }
      else {
        $ret = array($ret);
      }
    }
  }
  return $ret;
}

