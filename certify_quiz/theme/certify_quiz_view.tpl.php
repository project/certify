<?php

/**
 * @file
 * Theme file for display of quiz conditions in a certificate
 */

?>
<?php echo l($condition->mynode->title, 'node/' . $condition->mynode->nid); ?>
<?php if (user_access('view own certificate progress') && !$condition->score->passed) { ?>
  <span class="certify_failed">(<?php echo t('not passed'); ?>)</span>
<?php } elseif (user_access('view own certificate progress') && $condition->score->passed) { ?>
  <span class="certify_passed">(<?php echo t('passed'); ?>)</span>
<?php } ?>
