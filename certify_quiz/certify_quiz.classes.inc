<?php

/**
 * @file
 * Classes for certify_quiz module
 *
 * This file contains the class(es) for handling quiz conditions for the certify
 * module
 */

class CertifyQuiz extends CertifyCondition {

  public $score; // Score object from Quiz module

  /**
   * Return form elements for node form
   */
  public static function certify_form(&$node) {
    $optionsq = quiz_get_all_titles();// !TODO organic groups filtering
    $form['selected_quizzes'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Quizzes to be part of this certificate'),
      '#options' => $optionsq,
      '#default_value' => Certify::getConditionsNidsFromNode($node, 'quiz'),
    );
    return $form;
  }

  /**
   * Process node form submit
   */
  public static function certify_insert(&$node) {
    foreach ($node->selected_quizzes as $quizkey => $quiz) {
      if ($quizkey === $quiz) {
        $quiznode = node_load($quiz);
        // @TODO Should probably be done using an API
        db_query("INSERT INTO {certify_conditions} (cid, vid, condtype, condnode, condnodev) VALUES (%d, %d, 'quiz', %d, %d)",
          $node->nid, $node->vid, $quiz, $quiznode->vid);
      }
    }
  }

  /**
   * Render condition for view
   */
  public function certify_view($node) {
    global $user;
    $this->fillProgress($user->uid);
    return theme('certify_quiz_view', $node, $this);
  }

  /**
   * Header string for output
   */
  public static function getViewHeader() {
    return t("Quizzes");
  }

  /**
   * Calculate progress data for this condition
   */
  public function fillProgress($uid = 0) {

    $this->loadConditionNode();

    // Fetch quiz result
    $quizscores = quiz_get_score_data(array((int)$this->condnode), $uid);
    if (isset($quizscores[$this->mynode->vid])) {

      $this->score = $quizscores[$this->mynode->vid];

// Attempt to circumvent http://drupal.org/node/1395134
      if (!$this->score->max_score)
        $this->score->max_score = 100;

      $this->score->achieved_score = $this->score->percent_score * $this->score->max_score / 100;
      $this->score->pass_score = $this->score->percent_pass * $this->score->max_score / 100;

      if (!$this->score->percent_score) {
        $this->started = FALSE;
      }
      else {
        $this->started = TRUE;
      }

      if ($this->score->percent_score >= $this->score->percent_pass) {
        $this->score->passed = TRUE;
        $this->completed = TRUE;
      }
      else {
        $this->score->passed = FALSE;
        $this->completed = FALSE;
      }

    }
    else { // No takes for that revision I guess
      $this->score = (object) array('passed' => FALSE, 'gotpoints' => 0, 'gotpointscapped' => 0, 'totalpoints' => 0, 'requiredpoints' => 0);
      $this->completed = FALSE;

      // Can this happen? Should we insert an empty quiz object here?
    }

    $this->gotpoints = $this->score->achieved_score;
    $this->gotpointscapped = $this->score->achieved_score > $this->score->pass_score ? $this->score->pass_score : $this->score->achieved_score;
    $this->totalpoints = $this->score->max_score;
    $this->requiredpoints = $this->score->pass_score;

  }

}