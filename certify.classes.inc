<?php

/**
 * @file
 * Classes for the Certify module
 */

/**
 * CLASS CertifyCondition
 */

abstract class CertifyCondition {

  public $cid;        // certificate node
  public $vid;        // certificate node revision
  public $condtype;   // condition type
  public $condnode;   // condition node
  public $condnodev;  // condition node revision
  public $mynode;

  public $started;
  public $completed;

  public $gotpoints;
  public $gotpointscapped;
  public $totalpoints;
  public $requiredpoints;

  public function __construct($dbobj) {
    $this->cid = $dbobj->cid;
    $this->vid = $dbobj->vid;
    $this->condtype = $dbobj->condtype;
    $this->condnode = $dbobj->condnode;
    $this->condnodev = $dbobj->condnodev;
    $this->mynode = NULL;
  }

  public static function getViewHeader() {
    return "";
  }

  public abstract function fillProgress($uid = 0);

  public function loadConditionNode() {

    if ($this->mynode) {
      return $this->mynode;
    }

    $this->mynode = node_load(array('vid' => $this->condnodev));
    return $this->mynode;
  }

  public function certify_delete(&$node) {
  }

}

/**
 * CLASS Certify
 */

class Certify {

  public $conditions;
  private $status;
  public $node;

  /**
   * General constructor for the Certify class
   */
  public function __construct(&$node) {

    $condtypes = certify_conditiontypes();
    $this->node = $node;
    $this->conditions = array();
    $rs = db_query('SELECT cid, vid, condtype, condnode, condnodev FROM {certify_conditions} WHERE vid = %d', $node->vid);
    if ($rs) {
      $i = 0;
      while ($data = db_fetch_object($rs)) {
        if (isset($condtypes["certify_" . $data->condtype])) { // If we have a class registered
          $classname = $condtypes["certify_" . $data->condtype];
          $this->conditions[] = new $classname($data);
        }
      }
    }
    $this->status = FALSE;
  }

  public function delete(&$node) {
    foreach ($this->conditions as &$cond) {
      $cond->certify_delete($node);
    }

  }

  /**
   * Returns an assosiative array with condition types as keys, and each value an array of those conditions
   */
  public function getConditionsByType() {
    $ret = array();
    foreach ($this->conditions as &$cond) {
      if (!isset($ret[$cond->condtype])) {
        $ret[$cond->condtype] = array();
      }
      $ret[$cond->condtype][] = $cond;
    }
    return $ret;
  }

  /**
   * For a given node, return array of nids for a specific condition type (or all)
   */
  public static function getConditionsNidsFromNode($node, $type = NULL) {
    if (!isset($node->certificate)) {
      return array();
    }
    else {
      return $node->certificate->getConditionsNids($type);
    }
  }

  /**
   * Return array of nids for a specific condition type (or all)
   */
  public function getConditionsNids($type = NULL) {
    $conds = array();

    foreach ($this->conditions as &$cond) {
      if ($type && $type == $cond->condtype) {
        $conds[] = $cond->condnode;
      }
      elseif (!$type) {
        $conds[] = $cond->condnode;
      }
    }
    return $conds;
  }

  /**
   * Returns the status object for this certificate
   * If the $uid passed is different from the logged in user, the content is not cached.
   */
  public function getStatus($uid = 0, $notcurrent = FALSE) {
    global $user;

    if (!$uid) {
      $uid = $user->uid;
    }

    if ($this->status && $uid == $user->uid) {
      return $this->status;
    }

    $status = (object) array('started' => FALSE, 'completed' => TRUE, 'gotpoints' => 0, 'gotpointscapped' => 0, 'totalpoints' => 0, 'requiredpoints' => 0, 'progress' => 0);

    foreach ($this->conditions as &$condition) {
      $condition->fillProgress($uid);
      $status->gotpoints += $condition->gotpoints;
      $status->gotpointscapped += $condition->gotpointscapped;
      $status->totalpoints += $condition->totalpoints;
      $status->requiredpoints += $condition->requiredpoints;
      if ($condition->started) {
        $status->started = TRUE;
      }
      if (!$condition->completed) {
        $status->completed = FALSE;
      }

    }

    if ($status->requiredpoints) {
      $status->progress = floor($status->gotpointscapped * 100 / $status->requiredpoints);
    }

    if ($status->progress > 100) { // Should not really be possible, but better safe than sorry
      $status->progress = 100;
    }

    // allow modification of status
    drupal_alter('certify_status', $status, $uid, $this->node);
    if ($uid == $user->uid) {
      $this->status = $status;
    }
    
    return $status;

  }

  /**
   * Handles a change from a condition
   *
   * @param $user
   *  The user that got the change
   */
  public function scoreChanged($uid = NULL) {
    global $user;

    if (!$uid) {
      $uid = $user->uid;
    }
    module_invoke_all('certify_scorechanged',$this, $uid);

    // NOP
  }

  /**
   *  Function used to collect an array of certificate nodes
   *
   * @param $nids
   *   An array of nids to fetch. If not given, all available certificates are fetched.
   * @param $uid
   *   An user ID to filter by. This goes largely ignored for now.
   *
   * @return
   *   Returns an array of certificate nodes, with vid (revision id) as key.
   */
  public static function getCertificates($nids = NULL, $uid = NULL) {
    global $user;

    $certificates = array();

    if (!$uid) {
      $uid = $user->uid;
    }

    if (!is_array($nids)) {
      // Populate certificates
      $cres = db_query(db_rewrite_sql("SELECT vid FROM {node} n WHERE n.type='certificate'"));
      if (!$cres) {
        return $certificates;
      }
      while ($cert = db_fetch_array($cres)) {
        if ($certnode = node_load(array('vid' => (int) $cert['vid']))) {
          $certificates[$cert['vid']] = $certnode;
        }
      }
    }
    else {
      foreach ($nids as $nid) {
        if ($cert = node_load($nid)) {
          $certificates[$cert->vid] = $cert;
        }
      }
    }

    return $certificates;
  }

}