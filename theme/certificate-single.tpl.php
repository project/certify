<?php

/**
 * @file
 * Theme file for certificate blocks
 *
 * This file shows a single certificate
 */

?>
<div class="certify_certificate">
<?php if ($showtitle) { ?>
  <p class="certify_title"><strong><?php echo check_plain($certificate->title); ?></strong></p>
<?php } ?>
<div class="certify_bar-border">
  <div class="certify_bar-fill">
<?php if ($certificate->certificate->getStatus()->completed) { ?>
	  <div class="certify_bar-bar complete" style="width:100%;">
	    <span class="progress">100%</span>
      <span class="completed">
        <a href="<?php echo url('certify/certify_user_page_generate_pdf_certificate/' . $certificate->nid, array()); ?>">
        <img src="<?php echo base_path() . drupal_get_path('module', 'certify') . '/theme/'; ?>pdf-icon.gif" alt="">
        <?php echo t('Download certificate'); ?>
        </a>
      </span>
	  </div>
<?php } else { ?>
	  <div class="certify_bar-bar" style="width:<?php echo $certificate->certificate->getStatus()->progress; ?>%;">
	    <span class="progress"><?php echo $certificate->certificate->getStatus()->progress; ?>%</span>
	  </div>
<?php } ?>
	</div>
</div>

<?php if (!$condensed) { ?>
  <p class="certify_blockbody"><em><?php echo check_markup($certificate->body); ?></em></p>
<?php } ?>
<?php if (user_access('view certificate')) { ?>
  <div class="certify_details"><?php echo l(t("Show details"), 'node/' . $certificate->nid);?></div>
  <br style="clear:both;" />
<?php } ?>
</div>