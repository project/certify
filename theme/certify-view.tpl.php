<?php

/**
 * @file
 * Theme file for the node view of certificates
 *
 * Lists conditions for a certificate, and the progress
 */

?>

<?php foreach ($conditionsbytype as $condtype => &$conditions) { ?>
  <h3><?php echo call_user_func(array(get_class($conditions[0]), 'getViewHeader')); ?></h3>
  <ul class="certify_condlist"></ul>
  <?php foreach ($conditions as &$condition) { ?>
    <li><?php echo $condition->certify_view($node); ?></li>
  <?php } ?>
  </ul>
<?php } ?>

