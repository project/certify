<?php

/**
 * @file
 * Theme file for listing certificates connected to a user
 *
 * This files builds a table listing all certificates that belong to a user.
 */

?>
<?php foreach ($certificates as $certificate) { ?>
  <?php echo theme("certifysingle", $certificate); ?>
<?php } ?>
