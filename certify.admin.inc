<?php

/**
 * @file
 * Administration callbacks for the certify module
 */



/**
 * Configure certify.
 */
function certify_admin_settings() {

  $datefmt = explode(' ', variable_get('date_format_short', 'm/d/Y'));
  $datefmt = variable_get('certify_dateformat', $datefmt[0]);


  $form['certify_certificate_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the path to the location where to store certificates.'),
    '#description' => t('Must be a location writable by the server, but not public. Preferable without trailing directory separator.'),
    '#default_value' => variable_get('certify_certificate_path', '/var/drupal-content/certify'),
    '#resizable' => FALSE,
  );

  $form['certify_pdftk_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to pdftk.'),
    '#description' => t('Enter the path where pdftk is installed on your server.'),
    '#default_value' => variable_get('certify_pdftk_path', '/usr/bin/pdftk'),
    '#resizable' => FALSE,
  );

  $form['certify_drm_passphrase'] = array(
    '#type' => 'textfield',
    '#title' => t('Password for Digital Rights Management.'),
    '#description' => t('Enter a password that will be needed to edit the PDF output files. If blank, certify will generate normal PDF files..'),
    '#default_value' => variable_get('certify_drm_passphrase', ''),
    '#resizable' => FALSE,
  );

  $form['certify_og_filter'] = array(
    '#type' => 'checkbox',
    '#title' => t('Filter on organic groups.'),
    '#description' => t('If this is set, and organic groups is available, available nodes will be filtered based on the current group context.'),
    '#default_value' => variable_get('certify_og_filter', TRUE),
  );

  $form['certify_dateformat'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the date format to use in the PDF certificates.'),
    '#description' => t('This is a standard PHP date format string that will be used for formatting the date in the PDF certificates.'),
    '#default_value' => $datefmt,
    '#resizable' => FALSE,
  );

  $form['certify_display_score'] = array(
    '#type' => 'select',
    '#title' => t('Score display'),
    '#description' => t('How the score should be displayed on certificates'),
    '#options' => array(
      'passed' => t('Display "Passed"'),
      'percentage' => t('Display the percentage of awarded points from the total.'),
      'points' => t('Display points as e.g. "76 of 90"'),
    ),
    '#default_value' => variable_get('certify_display_score', 'passed'),
  );

  return system_settings_form($form);

}

/**
 * Validate the certify configuration form
 */
function certify_admin_settings_validate($form, $form_state) {

  $certificate_path = $form_state['values']['certify_certificate_path'];
  if (!is_dir($certificate_path)) {
    form_set_error('certify_certificate_path', t('Please select an existing directory.'));
  }
  if (!is_writable($certificate_path)) {
    form_set_error('certify_certificate_path', t('The directory needs to be writable for the webserver.'));
  }

  $pdftkpath = $form_state['values']['certify_pdftk_path'];
  if (!file_exists($pdftkpath) || !is_executable($pdftkpath)) {
    form_set_error('certify_pdftk_path', t('Please enter full path of pdftk executable.'));
  }
}

  

