<?php

/**
 * @file
 * Theme file for display of book conditions in a certificate
 */

?>


<?php echo l($condition->mynode->title, 'node/' . $condition->mynode->nid); ?>
<?php if (user_access('view own certificate progress') && $condition->completed) { ?>
  <span class="certify_passed">(<?php echo t('read'); ?>)</span>
<?php } elseif (user_access('view own certificate progress') && !$condition->completed) { ?>
  <?php if ($condition->showpages) { ?>
    <span class="certify_failed"><?php echo t('Unread pages:'); ?></span><ul>
      <?php foreach ($condition->pages as $page) { ?>
        <?php if ($page['bid'] != $page['nid']) { ?>
          <?php if (!$page['read']) { ?>
            <li><?php echo l($page['title'], 'node/' . $page['nid']);?></li>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </ul>
  <?php } else { ?>
    <span class="certify_failed">(<?php echo t('too many unread pages to show'); ?>)</span>
  <?php } ?>
<?php } ?>