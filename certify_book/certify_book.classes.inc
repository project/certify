<?php

/**
 * @file
 * Classes for certify_book module
 *
 * This file contains the class(es) for handling book conditions for the certify
 * module
 */

class CertifyBook extends CertifyCondition {

  public $showpages; // !! Whether to list pages on output
  public $pages; // Array of pages
  public $numpages; // Number of pages that has to be read

  public $pagesread;
  public $unreadpages;

  /**
   * Return form elements for node form
   */
  public static function certify_form(&$node) {

    // Collect books from the book-module
    $optionsb = array();
    $books = book_get_books();// !TODO organic groups filtering
    foreach ($books as $bookid => $book) {
      $optionsb[$bookid] = $book['title'];
    }

    // Books
    $form['selected_books'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Books to be part of this certificate'),
      '#options' => $optionsb,
      '#default_value' => Certify::getConditionsNidsFromNode($node, 'book'),
    );
    return $form;
  }

  /**
   * Process node form submit
   */
  public static function certify_insert(&$node) {
    foreach ($node->selected_books as $bookkey => $book) {
      if ($bookkey === $book) {
        $booknode = node_load($book);
        // @TODO Should probably be done using an API
        db_query("INSERT INTO {certify_conditions} (cid, vid, condtype, condnode, condnodev) VALUES (%d, %d, 'book', %d, %d)",
          $node->nid, $node->vid, $book, $booknode->vid);
      }
    }
  }

  /**
   * Handle deletion of certificate
   */
  public function certify_delete(&$node) {
    // @TODO purge booklog entries ONLY if this node is the last referencing the selected book id
  }

  /**
   * Render condition for view
   */
  public function certify_view($node) {
    global $user;
    $this->fillProgress($user->uid);
    return theme('certify_book_view', $node, $this);
  }

  /**
   * Header string for output
   */
  public static function getViewHeader() {
    return t("Books");
  }

  /**
   * Calculate progress data for this condition
   */
  public function fillProgress($uid = 0) {

    $this->loadConditionNode();

    $bpages = book_toc($this->mynode->book['bid'], array(), 999);
    $bpres = db_query('SELECT mlid,b.nid,n.title FROM {book} b INNER JOIN {node} n ON n.nid = b.nid AND n.status = 1 WHERE b.bid = %d',
      $this->mynode->book['bid']);
    $pages = array();
    while ($page = db_fetch_array($bpres)) { // Iterate over all pages
      $pages[$page['nid']] = array(
        'title' => check_plain($page['title']),
        'mlid' => $page['mlid'],
        'nid' => $page['nid'],
        'bid' => $this->mynode->book['bid'],
        'read' => FALSE
      );
    }
    $this->pages = $pages;
    $this->numpages = count($pages) - 1; // Excluding front page

  // Book calculations (assumes front page is shown)

    $this->pagesread = 0;
    $logres = db_query('SELECT nid FROM {certify_book_viewlog} WHERE uid = %d AND bid = %d', $uid, $this->mynode->book['bid']);
    while ($page = db_fetch_array($logres)) {
      if ($this->mynode->book['bid'] != $page['nid']) { // Do not count front page
        $this->pages[$page['nid']]['read'] = TRUE;
        $this->pagesread++;
      }
    }

    $this->unreadpages = $this->numpages - $this->pagesread;
    if ($this->numpages == 0) {
      $bookpct = 100; // All read
    }
    else {
      $bookpct = ( (float) $this->pagesread / (float) $this->numpages ) * 100;
    }
    if ( ( $this->unreadpages < 10 ) || ( $bookpct > 90 ) ) {
      $this->showpages = TRUE;
    }
    else {
      $this->showpages = FALSE;
    }

    if ($this->pagesread) {
      $this->started = TRUE;
    }
    else {
      $this->started = FALSE;
    }

    if ($this->unreadpages > 0) {
      $this->completed = FALSE;
    }
    else {
      $this->completed = TRUE;
    }

    $this->gotpoints = $this->pagesread;
    $this->gotpointscapped = $this->pagesread;
    $this->totalpoints = $this->numpages;
    $this->requiredpoints = $this->numpages;

//    dpm($this, 'condition after fill with ' . $uid . "," . $this->mynode->book['bid']);


  }
}